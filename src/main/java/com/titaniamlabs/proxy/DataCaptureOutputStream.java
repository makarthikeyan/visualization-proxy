package com.titaniamlabs.proxy;

import java.io.*;

public class DataCaptureOutputStream extends OutputStream {

    private OutputStream base;
    private ByteArrayOutputStream dataCapture;

    public DataCaptureOutputStream(OutputStream base) throws IOException {
        this.base = base;
        this.dataCapture = new ByteArrayOutputStream();
    }

    @Override
    public void write(int b) throws IOException {
        base.write(b);
        dataCapture.write(b);
    }

    @Override
    public void write(byte[] b) throws IOException {
        base.write(b);
        dataCapture.write(b);
    }

    @Override
    public void write(byte[] b, int off, int len) throws IOException {
        base.write(b, off, len);
        dataCapture.write(b, off, len);
    }

    @Override
    public void flush() throws IOException {
        base.flush();
        dataCapture.flush();
    }

    @Override
    public void close() throws IOException {
        base.close();
    }

    public String data() {
        return dataCapture.toString();
    }
}
