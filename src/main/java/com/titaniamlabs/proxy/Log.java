package com.titaniamlabs.proxy;

import java.util.function.Supplier;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Log {

    static {
        System.setProperty("java.util.logging.SimpleFormatter.format",
                "[%1$tF %1$tT] [%4$-1s] %5$s %n"
        );
    }

    public static Log getLogger(Class klass) {
        return new Log(klass);
    }


    private final Logger logger;
    private static final ThreadLocal<Boolean> disableInThread = new ThreadLocal<>();

    private Log(Class klass) {

        this.logger = Logger.getLogger(klass.getName());
        disableInThread.set(Boolean.FALSE);
    }

    public void info(String message) {
        if(disableInThread.get()==null || !disableInThread.get()) {
            logger.log(Level.INFO, message);
        }
    }

    public void info(Supplier<String> supplier) {
        if(disableInThread.get()==null || !disableInThread.get()) {
            logger.log(Level.INFO, supplier);
        }
    }

    public void disableInThread(boolean disable) {
        disableInThread.set(disable);
    }

    public void warn(String message, Throwable t) {
        logger.log(Level.WARNING, message, t);
    }

    public void warn(String message) {
        logger.log(Level.WARNING, message);
    }


}
