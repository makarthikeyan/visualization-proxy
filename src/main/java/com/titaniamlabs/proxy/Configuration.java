package com.titaniamlabs.proxy;

public class Configuration {

    private boolean logInput;
    private boolean logOutput;
    private boolean logHeaders;

    private static Configuration instance;

    public static void initialize(String configAsString) {
        instance = new Configuration(configAsString);
    }

    public static Configuration getInstance() {
        if(instance==null) {
            throw new IllegalStateException("Call initialize() before calling getInstance()");
        }
        return instance;
    }

    private Configuration(String configAsString) {
        logInput = configAsString.contains("input");
        logOutput = configAsString.contains("output");
        logHeaders = configAsString.contains("headers");
    }

    public boolean isLogInput() {
        return logInput;
    }

    public boolean isLogOutput() {
        return logOutput;
    }

    public boolean isLogHeaders() {
        return logHeaders;
    }

    public boolean isLogEnabled() {
        return logInput || logOutput || logHeaders;
    }
}
