package com.titaniamlabs.proxy;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class DataCaptureInputStream extends InputStream {

    private InputStream base;
    private ByteArrayOutputStream baos;


    public DataCaptureInputStream(InputStream base) {
        this.base = base;
    }

    @Override
    public int read() throws IOException {
        return base.read();
    }

    @Override
    public int read(byte[] b) throws IOException {
        return base.read(b);
    }

    @Override
    public int read(byte[] b, int off, int len) throws IOException {
        return base.read(b, off, len);
    }

    @Override
    public byte[] readAllBytes() throws IOException {
        return base.readAllBytes();
    }

    @Override
    public byte[] readNBytes(int len) throws IOException {
        return base.readNBytes(len);
    }

    @Override
    public int readNBytes(byte[] b, int off, int len) throws IOException {
        return base.readNBytes(b, off, len);
    }

    @Override
    public long skip(long n) throws IOException {
        return base.skip(n);
    }

    @Override
    public int available() throws IOException {
        return base.available();
    }

    @Override
    public void close() throws IOException {
        base.close();
    }

    @Override
    public synchronized void mark(int readlimit) {
        base.mark(readlimit);
    }

    @Override
    public synchronized void reset() throws IOException {
        base.reset();
    }

    @Override
    public boolean markSupported() {
        return base.markSupported();
    }

    @Override
    public long transferTo(OutputStream out) throws IOException {
        return base.transferTo(out);
    }
}
