package com.titaniamlabs.proxy;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.*;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

public class ProxyRequestHandler implements HttpHandler {

    private static final Log log = Log.getLogger(ProxyRequestHandler.class);

    private static final String INPUT_MARKER = "-->";
    private static final String OUTPUT_MARKER = "<--";

    private static final Set<String> HEADERS_SKIPLIST = Set.of("Connection", "Host","Upgrade","Content-length","User-agent");
    private static final Set<String> DO_NOT_LOG_URLS = Set.of(
            "filter_path=nodes.*.version%2Cnodes.*.http.publish_address%2Cnodes.*.ip"
    );

    private static final Map<String,String> replaceMap = Map.of("tangled_ip","ip",
            "tangled_keyword","keyword",
            "keyword_masked","keyword",
            "tangled_tiny_keyword","keyword",
            "tangled_tiny_text","text",
            "tangled_keyword_masked","keyword");

    private static final Map<String,Pattern> replacePatterns = Map.of("tangled_ip",Pattern.compile("tangled_ip"),
            "tangled_keyword",Pattern.compile("tangled_keyword"),
            "tangled_tiny_keyword",Pattern.compile("tangled_tiny_keyword"),
            "keyword_masked",Pattern.compile("keyword_masked"),
            "tangled_keyword_masked",Pattern.compile("tangled_keyword_masked"),
            "tangled_tiny_text",Pattern.compile("tangled_tiny_text")
    );


    //=====================================//
    private HttpClient client;

    private String proxyTarget;

    private Configuration configuration;

    public ProxyRequestHandler(String proxyTarget, Configuration configuration) {

        this.client = HttpClient.newBuilder()
                .version(HttpClient.Version.HTTP_2)
                .followRedirects(HttpClient.Redirect.NORMAL)
        .build();

        this.proxyTarget = proxyTarget;
        this.configuration = configuration;
    }

    /**
     * Contract - hook to the server.
     * @param exchange
     * @throws IOException
     */
    @Override
    public void handle(HttpExchange exchange) throws IOException {

        initLogger(exchange);

        log.info("vvvvvvvvvvvvvvvvvvvvvvvvvvvv");

        try {

            doProxy(exchange);

        } catch (Exception e) {

            handleException(exchange, e);
        } finally {
            exchange.getResponseBody().close();
        }

        log.info("^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
    }

    /**
     * Reduces noise in logging of requests..
     *
     * @param exchange
     */
    public void initLogger(HttpExchange exchange) {

        String query = exchange.getRequestURI()!=null ? exchange.getRequestURI().getRawQuery() : null;

        if(query!=null) {
            log.disableInThread(DO_NOT_LOG_URLS.contains(query));
        }

        //log.info(exchange.getRequestURI()!=null ? exchange.getRequestURI().getRawQuery() : "");
    }

    /**
     * Responsible for handling any exceptions that might happen during request processing
     * @param exchange
     * @param e
     * @throws IOException
     */
    public void handleException(HttpExchange exchange, Exception e) throws IOException {

        log.warn("Request Failed: "+e.getMessage(), e);
        exchange.sendResponseHeaders(500,0);
        exchange.getResponseBody().flush();
        exchange.getResponseBody().close();
    }

    /**
     * Do the proxy exchange.
     *
     * @param exchange
     * @throws Exception
     */
    public void doProxy(HttpExchange exchange) throws IOException {

        try {
            // 1. build proxy request
            HttpRequest targetRequest = buildTargetRequest(exchange);
            //log.info("targetRequest built");

            // 2. execute proxy and get proxy response
            HttpResponse<InputStream> targetResponse = client.send(targetRequest, HttpResponse.BodyHandlers.ofInputStream());
            //log.info("targetResponse received");

            // 3. send the response back to client.
            writeProxyResponseToSource(exchange, targetResponse);

            if(configuration.isLogEnabled()) {
                log.info("wrote response back to source");
            }

        } catch (InterruptedException e) {

            throw new IOException(e);
        }
    }

    /**
     * Builds the proxy request to the target location
     *
     * @param exchange
     * @return
     */
    public HttpRequest buildTargetRequest(HttpExchange exchange) {

        URI proxyUri = createTargetUri(exchange.getRequestURI());

        log.info(exchange.getRequestMethod()+" : "+proxyUri);

        HttpRequest.Builder httpRequestBuilder = HttpRequest.newBuilder(proxyUri)
                .header("User-Agent","Titaniam-Proxy")
                .method(exchange.getRequestMethod(), HttpRequest.BodyPublishers.ofInputStream(()-> logStream(exchange.getRequestBody())));

        copyHeadersToTarget(exchange, httpRequestBuilder);

        return httpRequestBuilder.build();
    }

    /**
     * Target URI, with all params from the input
     *
     * @param originalUri
     * @return
     */
    public URI createTargetUri(URI originalUri) {

        String path = originalUri.getRawPath();
        path = path==null ? "" : path;

        String query = originalUri.getRawQuery();
        query = query==null ? "" : "?"+query;

        return URI.create(proxyTarget+path+query);

    }

    /**
     * Copy any incoming headers to the proxy host. Check what needs to be blacklisted.
     *
     * @param exchange
     * @param httpRequestBuilder
     */
    public void copyHeadersToTarget(HttpExchange exchange, HttpRequest.Builder httpRequestBuilder) {

        Headers headers = exchange.getRequestHeaders();

        if(headers==null || headers.size()==0) {
            log.info("no headers to copy to target");
            return;
        }

        for(Map.Entry<String, List<String>> header : headers.entrySet()) {
            String name = header.getKey();
            if(!HEADERS_SKIPLIST.contains(name)) {
                for(String value : header.getValue()) {

                    if(configuration.isLogHeaders()) {
                        log.info("-->header: "+name+","+value);
                    }

                    httpRequestBuilder.header(name, value);
                }
            }
        }
    }

    /**
     * Write the response back, manipulating as required.
     *
     * @param exchange
     * @param proxyResponse
     * @throws IOException
     */
    public void writeProxyResponseToSource(HttpExchange exchange, HttpResponse<InputStream> proxyResponse) throws IOException{

        long contentLength = 0L;

        // response body - nice with java 9!
        PatchedStream patchedStream = fixIt(exchange, proxyResponse.body());
        contentLength = patchedStream !=null && patchedStream.contentLength>0 ? patchedStream.contentLength : 0L;

        // response headers
        for(Map.Entry<String, List<String>> header : proxyResponse.headers().map().entrySet()) {

            if(configuration.isLogHeaders()) {
                log.info("<--header: "+header.getKey()+","+header.getValue());
            }

            exchange.getResponseHeaders().put(header.getKey(), header.getValue());

            if(contentLength<=0) {
                if(header.getKey().equalsIgnoreCase("content-length")) {
                    contentLength = Long.parseLong(header.getValue().get(0));
                }
            }
        }

        // write this after writing other headers, else , nothing will go through
        exchange.sendResponseHeaders(proxyResponse.statusCode(), contentLength);

        if(configuration.isLogOutput()) {
            DataCaptureOutputStream dataCaptureOutputStream = new DataCaptureOutputStream(exchange.getResponseBody());
            patchedStream.transferTo(dataCaptureOutputStream);
            dataCaptureOutputStream.flush();
            dataCaptureOutputStream.close();
            log.info(OUTPUT_MARKER);
            log.info(dataCaptureOutputStream.data());
        } else {
            patchedStream.transferTo(exchange.getResponseBody());
            exchange.getResponseBody().flush();
            exchange.getResponseBody().close();
        }
    }

    /**
     * Returns true if this response needs to be fixed before client sees the contents.
     *
     * @param exchange
     * @return
     */
    public boolean needsFixing(HttpExchange exchange) {

        // http://172.32.67.193:9200/sessions2-200913*/_field_caps?fields=*&ignore_unavailable=true&allow_no_indices=false

        String path = exchange!=null && exchange.getRequestURI()!=null ? exchange.getRequestURI().getRawPath() : "";

        return path.endsWith("_field_caps");
    }

    /**
     * Given an original stream of response from proxy, returns
     * a stream with values appropriately changed.
     *
     * @param original
     * @return
     */
    public PatchedStream fixIt(HttpExchange exchange, InputStream original) throws IOException {

        if(needsFixing(exchange)) {

            ByteArrayOutputStream baos = new ByteArrayOutputStream();

            original.transferTo(baos);

            String result = replaceTangledFields(baos.toString());
            byte[] bytes = result.getBytes();

            return new PatchedStream(new ByteArrayInputStream(bytes), bytes.length);

        } else {
            return new PatchedStream(original, -1);
        }
    }

    /**
     *
     * @param original
     * @return
     */
    public String replaceTangledFields(String original) {

        String processed = original;

        for(String actualKey : replacePatterns.keySet()) {
            processed = replacePatterns.get(actualKey).matcher(processed).replaceAll(replaceMap.get(actualKey));
        }

        return processed;
    }

    /**
     * Logs the input contents, for debugging purposes.
     *
     * @param stream
     * @return
     */
    public InputStream logStream(InputStream stream)  {

        if(stream==null) {
            return null;
        }

        if(configuration.isLogInput()) {

            ByteArrayOutputStream baos = new ByteArrayOutputStream();

            try {
                stream.transferTo(baos);
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }

            String contents = baos.toString();

            log.info(INPUT_MARKER);
            log.info(contents);

            return new ByteArrayInputStream(contents.getBytes());

        } else {

            return stream;
        }
    }

    /**
     * Stream that holds a patched version and the resulting updated content length
     */
    public class PatchedStream {

        public PatchedStream(InputStream stream, int contentLength) {
            this.stream = stream;
            this.contentLength = contentLength;
        }

        public InputStream stream;
        public int contentLength = -1;

        public void transferTo(OutputStream outputStream) throws IOException {
            stream.transferTo(outputStream);
        }
    }



}
