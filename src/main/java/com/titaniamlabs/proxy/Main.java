package com.titaniamlabs.proxy;

import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.logging.Logger;

public class Main {

    private static final Log logger = Log.getLogger(Main.class);

    public static void main(String[] args) throws Exception {

        if(args==null || args.length<2) {
            System.out.println("Usage: java -jar titaniam-proxy.jar <<proxy_daemon_port>> <<target_host>>");
            System.out.println("   Ex: java -jar titaniam-proxy.jar 6969 http://localhost:9200");
            return;
        }

        // initialize configuration
        if(args.length==3) {
            Configuration.initialize(args[2]);
        } else {
            Configuration.initialize("");
        }

        System.out.println("Starting..");

        int proxyDaemonPort = Integer.parseInt(args[0]);
        String targetHostPort  = args[1].trim();

        Main main = new Main(proxyDaemonPort, targetHostPort);
        main.start();
    }

    //====================================//

    private HttpServer server;

    public Main(int proxyDaemonPort, String targetHostPort) throws IOException {

        ThreadPoolExecutor threadPoolExecutor = (ThreadPoolExecutor) Executors.newFixedThreadPool(10);

        server = HttpServer.create(new InetSocketAddress(proxyDaemonPort), 0);
        server.createContext("/", new ProxyRequestHandler(targetHostPort, Configuration.getInstance()));
        server.setExecutor(threadPoolExecutor);

        Runtime.getRuntime().addShutdownHook(new Thread(this::stop));
    }

    public void start() {

        server.start();
        logger.warn("Proxy started on port "+server.getAddress().getPort());
    }

    public void stop() {
        logger.warn("Proxy Stopped");
        server.stop(0);
    }

}
