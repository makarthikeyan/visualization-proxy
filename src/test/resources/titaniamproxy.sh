#!/bin/sh
SERVICE_NAME=titaniamproxy
PATH_TO_JAR=/home/karthik/lib/titaniam-proxy.jar
PID_PATH_NAME=/tmp/titaniamproxy-pid
case $1 in
start)
       echo "Starting $SERVICE_NAME ..."
  if [ ! -f $PID_PATH_NAME ]; then
       nohup /home/karthik/bin/jdk-12.0.2/bin/java -jar $PATH_TO_JAR 6969 http://172.32.67.193:9200 2>> /var/log/titaniamproxy/titaniamproxy.err >>/var/log/titaniamproxy/titaniamproxy.out &
       echo $! > $PID_PATH_NAME
       echo "$SERVICE_NAME started ..."
  else
       echo "$SERVICE_NAME is already running ..."
  fi
;;
stop)
  if [ -f $PID_PATH_NAME ]; then
         PID=$(cat $PID_PATH_NAME);
         echo "$SERVICE_NAME stoping ..."
         kill $PID;
         echo "$SERVICE_NAME stopped ..."
         rm $PID_PATH_NAME
  else
         echo "$SERVICE_NAME is not running ..."
  fi
;;
restart)
  if [ -f $PID_PATH_NAME ]; then
      PID=$(cat $PID_PATH_NAME);
      echo "$SERVICE_NAME stopping ...";
      kill $PID;
      echo "$SERVICE_NAME stopped ...";
      rm $PID_PATH_NAME
      echo "$SERVICE_NAME starting ..."
      nohup java -jar $PATH_TO_JAR /tmp 2>> /dev/null >> /dev/null &
      echo $! > $PID_PATH_NAME
      echo "$SERVICE_NAME started ..."
  else
      echo "$SERVICE_NAME is not running ..."
     fi     ;;
 esac

#https://medium.com/@ameyadhamnaskar/running-java-application-as-a-service-on-centos-599609d0c641
