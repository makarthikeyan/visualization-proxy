package com.titaniamlabs.proxy;

import com.google.gson.Gson;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.util.logging.Logger;

import static org.testng.Assert.assertEquals;

public class MainTest {

    static {
        System.setProperty("java.util.logging.SimpleFormatter.format",
                "[%1$tF %1$tT] [%4$-1s] %5$s %n"
        );
    }

    public static final int PROXY_PORT = 6969;

    private static final Logger log = Logger.getLogger(MainTest.class.getName());

    private TestTargetServer targetServer;
    private Main main;

    @BeforeClass
    public void oneTimeSetup() throws Exception {

        Configuration.initialize("");
        targetServer = new TestTargetServer();
        // 1. start the proxy server pointing to the target
        main = new Main(PROXY_PORT,"http://localhost:"+TestTargetServer.TARGET_SERVER_PORT);
        main.start();
    }

    @AfterClass
    public void oneTimeTearDown() {

        if(targetServer !=null) {
            targetServer.stop(0);
        }

        if(main!=null) {
            main.stop();
        }
    }

    @Test
    public void testSimple() throws Exception {

        makeTheCall("",
                "{\"query\":{\"match_all\":{}}}",
                "/",
                null);

        makeTheCall("/_cat/indices",
                "{\"query\":{\"match_all\":{}}}",
                "/_cat/indices",
                null);

        makeTheCall("/_doc/index1?pipeline=abc",
                "",
                "/_doc/index1?pipeline=abc",
                null);
    }

    public void makeTheCall(String path, String requestBody, String expectedUrl, String expectedBody) throws Exception {

        // 2. Execute a http request to the proxy that should go to target and come back.
        HttpClient httpClient = HttpClient.newBuilder().build();

        HttpRequest httpRequest = HttpRequest.newBuilder(URI.create("http://localhost:"+PROXY_PORT+path))
                .timeout(Duration.ofMinutes(1))
                .header("Content-Type", "application/json")
                .header("Accept", "application/json")
                .POST(HttpRequest.BodyPublishers.ofString(requestBody))
                .build();

        HttpResponse<String> response = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
        assertEquals(response.statusCode(), 200);
        assertEquals(response.headers().firstValue("X-Special-Proxy-Header".toLowerCase()).get(), "monkey");

        String responseBody = response.body();

        log.info(responseBody);

        Gson gson = new Gson();
        ResponseObject responseObject = gson.fromJson(responseBody, ResponseObject.class);

        assertEquals(responseObject.url, expectedUrl);
        assertEquals(responseObject.body, expectedBody!=null ? expectedBody : requestBody);
        assertEquals(responseObject.headers, "Accept:application/json,Connection:Upgrade, HTTP2-Settings,Http2-settings:AAEAAEAAAAIAAAABAAMAAABkAAQBAAAAAAUAAEAA,Host:localhost:7777,Upgrade:h2c,User-agent:Titaniam-Proxy,Transfer-encoding:chunked,Content-type:application/json");
    }

    @Test
    public void testReplacement() throws Exception {

        makeTheCall("/sessions2-200913*/_field_caps",
                "    \"dstMac\": {\n" +
                        "      \"tangled_keyword\": {\n" +
                        "        \"type\": \"tangled_keyword\",\n" +
                        "        \"searchable\": true,\n" +
                        "        \"aggregatable\": true\n" +
                        "      }\n" +
                        "    }",
                "/sessions2-200913*/_field_caps",
                "    \"dstMac\": {\n" +
                        "      \"keyword\": {\n" +
                        "        \"type\": \"keyword\",\n" +
                        "        \"searchable\": true,\n" +
                        "        \"aggregatable\": true\n" +
                        "      }\n" +
                        "    }"
                );

    }

}
