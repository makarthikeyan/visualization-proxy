package com.titaniamlabs.proxy;

import com.sun.net.httpserver.HttpExchange;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.*;
import java.lang.annotation.Retention;
import java.net.URI;
import java.net.URL;
import java.net.http.HttpHeaders;
import java.net.http.HttpResponse;
import java.util.List;
import java.util.Map;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class ProxyRequestHandlerTest {

    private static final Log log = Log.getLogger(ProxyRequestHandlerTest.class);

    private TestTargetServer targetServer;
    private ProxyRequestHandler proxyHandler;

    @BeforeClass
    public void oneTimeSetup() throws Exception {

        targetServer = new TestTargetServer();
    }

    @AfterClass
    public void oneTimeTearDown() {

        if(targetServer !=null) {
            targetServer.stop(0);
        }

        log.info("test server started");
    }

    @BeforeMethod
    public void setup() {
        Configuration configuration = mock(Configuration.class);
        proxyHandler = new ProxyRequestHandler("http://localhost:"+TestTargetServer.TARGET_SERVER_PORT, configuration);
    }

    @Test
    public void testWriteProxyResponseToSource() throws Exception {


        HttpHeaders headers = HttpHeaders.of(Map.of("key1", List.of("value1")), (x,y) -> true);

        HttpExchange httpExchange = mock(HttpExchange.class);
        HttpResponse<InputStream> proxyResponse = (HttpResponse<InputStream>)mock(HttpResponse.class);
        OutputStream responseBody = mock(OutputStream.class);
        InputStream  proxyResponseBody = mock(InputStream.class);

        com.sun.net.httpserver.Headers forward = mock(com.sun.net.httpserver.Headers.class);

        when(proxyResponse.statusCode()).thenReturn(555);
        when(proxyResponse.headers()).thenReturn(headers);
        when(httpExchange.getResponseBody()).thenReturn(responseBody);
        when(proxyResponse.body()).thenReturn(proxyResponseBody);

        when(httpExchange.getResponseHeaders()).thenReturn(forward);

        proxyHandler.writeProxyResponseToSource(httpExchange, proxyResponse);
    }

    @Test
    public void testDoProxy() throws Exception {

        HttpExchange exchange = mock(HttpExchange.class);

        URI requestURI = new URL("http://localhost:6969/_cat/indices?pipeline=monkey").toURI();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        com.sun.net.httpserver.Headers forward = mock(com.sun.net.httpserver.Headers.class);
        when(forward.size()).thenReturn(0);

        when(exchange.getRequestURI()).thenReturn(requestURI);
        when(exchange.getRequestMethod()).thenReturn("POST");
        when(exchange.getRequestBody()).thenReturn(new ByteArrayInputStream("{abc:def}".getBytes()));
        when(exchange.getRequestHeaders()).thenReturn(forward);
        when(exchange.getResponseHeaders()).thenReturn(forward);
        when(exchange.getResponseBody()).thenReturn(baos);

        proxyHandler.doProxy(exchange);

        String response = baos.toString();
        assertTrue(response.contains( "\"url\":\"/_cat/indices?pipeline\\u003dmonkey\""));
        assertTrue(response.contains("\"body\":\"{abc:def}\""));
    }

    @Test
    public void testCreateTargetUri() {

        URI uri = URI.create("http://localhost/_cat/indices?abc=de+f");
        URI actual = proxyHandler.createTargetUri(uri);
        assertEquals(actual.toASCIIString(), "http://localhost:"+TestTargetServer.TARGET_SERVER_PORT+"/_cat/indices?abc=de+f");

        uri = URI.create("http://localhost/");
        actual = proxyHandler.createTargetUri(uri);
        assertEquals(actual.toASCIIString(), "http://localhost:"+TestTargetServer.TARGET_SERVER_PORT+"/");
    }

    @Test
    public void testNeedsFixing() {

        URI uri = URI.create("http://172.32.67.193:9200/sessions2-200913*/_field_caps?fields=*&ignore_unavailable=true&allow_no_indices=false");
        HttpExchange exchange = mock(HttpExchange.class);
        when(exchange.getRequestURI()).thenReturn(uri);

        assertTrue(proxyHandler.needsFixing(exchange));
    }

    @Test
    public void testReplaceString() {

        String tobeReplaced = "    \"dstMac\": {\n" +
                "      \"tangled_keyword\": {\n" +
                "        \"type\": \"tangled_keyword\",\n" +
                "        \"searchable\": true,\n" +
                "        \"aggregatable\": true\n" +
                "      }\n" +
                "    }";

        assertEquals(proxyHandler.replaceTangledFields(tobeReplaced),"    \"dstMac\": {\n" +
                "      \"keyword\": {\n" +
                "        \"type\": \"keyword\",\n" +
                "        \"searchable\": true,\n" +
                "        \"aggregatable\": true\n" +
                "      }\n" +
                "    }");
    }

    @Test
    public void testReplaceMulti() {

        String tobeReplaced = "    \"radius.framedIp\": {\n" +
                "      \"tangled_ip\": {\n" +
                "        \"type\": \"tangled_ip\",\n" +
                "        \"searchable\": true,\n" +
                "        \"aggregatable\": true\n" +
                "      }\n" +
                "    },\n" +
                "    \"socks.ASN\": {\n" +
                "      \"keyword\": {\n" +
                "        \"type\": \"keyword\",\n" +
                "        \"searchable\": true,\n" +
                "        \"aggregatable\": true\n" +
                "      }\n" +
                "    },\n" +
                "    \"srcMac\": {\n" +
                "      \"tangled_keyword\": {\n" +
                "        \"type\": \"tangled_keyword\",\n" +
                "        \"searchable\": true,\n" +
                "        \"aggregatable\": true\n" +
                "      }\n" +
                "    }";

        String expected = "    \"radius.framedIp\": {\n" +
                "      \"ip\": {\n" +
                "        \"type\": \"ip\",\n" +
                "        \"searchable\": true,\n" +
                "        \"aggregatable\": true\n" +
                "      }\n" +
                "    },\n" +
                "    \"socks.ASN\": {\n" +
                "      \"keyword\": {\n" +
                "        \"type\": \"keyword\",\n" +
                "        \"searchable\": true,\n" +
                "        \"aggregatable\": true\n" +
                "      }\n" +
                "    },\n" +
                "    \"srcMac\": {\n" +
                "      \"keyword\": {\n" +
                "        \"type\": \"keyword\",\n" +
                "        \"searchable\": true,\n" +
                "        \"aggregatable\": true\n" +
                "      }\n" +
                "    }";

        assertEquals(proxyHandler.replaceTangledFields(tobeReplaced), expected);
    }

}
