package com.titaniamlabs.proxy;

import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class TestTargetServer {

    private static final Logger log = Logger.getLogger(TestTargetServer.class.getName());

    public static final int TARGET_SERVER_PORT = 7777;
    private HttpServer server;

    public TestTargetServer() throws IOException {

        ThreadPoolExecutor threadPoolExecutor = (ThreadPoolExecutor) Executors.newFixedThreadPool(1);

        server = HttpServer.create(new InetSocketAddress("localhost", TARGET_SERVER_PORT), 0);
        server.createContext("/", new TestHttpHandler());
        server.setExecutor(threadPoolExecutor);
        server.start();

        log.info("Target server started");
    }

    public void stop(int delay){
        server.stop(delay);
    }

    public static class TestHttpHandler implements HttpHandler {

        @Override
        public void handle(HttpExchange exchange) throws IOException {

            log.info("target server got the request");

            String response = buildResponse(exchange);

            exchange.getResponseHeaders().put("X-Special-Proxy-Header", Arrays.asList("monkey"));
            exchange.sendResponseHeaders(200, response.getBytes().length);
            exchange.getResponseBody().write(response.getBytes());
            exchange.getResponseBody().flush();
            exchange.getResponseBody().close();
        }

        public String buildResponse(HttpExchange exchange) {

            String requestContents = new BufferedReader(new InputStreamReader(exchange.getRequestBody()))
                    .lines().collect(Collectors.joining("\n"));

            ResponseObject responseObject = new ResponseObject();
            responseObject.body = requestContents;
            responseObject.url = exchange.getRequestURI().toASCIIString();
            responseObject.headers = mapAsString(exchange.getRequestHeaders());

            Gson gson = new Gson();
            return gson.toJson(responseObject);
        }

        public String mapAsString(Map<String, List<String>> map) {

            StringBuilder stringBuilder = new StringBuilder();

            for(Map.Entry<String,List<String>> entry : map.entrySet()) {

                if(stringBuilder.length()>0) {
                    stringBuilder.append(',');
                }

                stringBuilder.append(entry.getKey())
                        .append(':')
                        .append(entry.getValue().get(0));
            }

            return stringBuilder.toString();
        }
    }
}
