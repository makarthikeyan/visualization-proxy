package com.titaniamlabs.proxy;

public class ResponseObject {

    public String url;
    public String body;
    public String headers;
}
