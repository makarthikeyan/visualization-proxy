package com.titaniamlabs.proxy;

import org.testng.annotations.Test;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;

import static org.testng.Assert.assertEquals;

public class DataCaptureOutputStreamTest {

    @Test
    public void testSimple() throws Exception {

        OutputStream base = new ByteArrayOutputStream();
        DataCaptureOutputStream dataCaptureOutputStream = new DataCaptureOutputStream(base);
        dataCaptureOutputStream.write("abc".getBytes());
        dataCaptureOutputStream.flush();
        dataCaptureOutputStream.close();

        assertEquals(dataCaptureOutputStream.data(), "abc");
    }

    @Test
    public void testNegative() throws Exception {

        OutputStream base = new ByteArrayOutputStream();
        DataCaptureOutputStream dataCaptureOutputStream = new DataCaptureOutputStream(base);
        dataCaptureOutputStream.flush();
        dataCaptureOutputStream.close();

        assertEquals(dataCaptureOutputStream.data(),"");
    }
}
